import React from "react";

export class Page extends React.Component {
	render() {
		console.log("Page render");
		return (
				<div className="pageinfo"> 
					<div>Page {this.props.page.number} of {this.props.page.totalPages - 1} Pages</div>
					<div>{this.props.page.totalElements} Records</div>
				</div>
		)
	}
}

export class Paginator extends React.Component {
	render() {
		console.log("Paginator render");
		
		return (
			<ul className="nav">
				{this.renderFirstButton()}
				{this.renderPrevButton()}
				{this.renderNextButton()}
				{this.renderLastButton()}
			</ul>
		)
	}
	
	renderFirstButton() {
		if (this.props.links.prev) {
			return (<li id="first" className="button nav" onClick={() => this.props.follow(this.props.links.first)} >{this.props.labels[0]}</li>)
		} 
		return (<li id="first" className="button nav disabled" >{this.props.labels[0]}</li>)
	}

	renderPrevButton() {
		if (this.props.links.prev) {
			return (<li id="prev" className="button nav" onClick={() => this.props.follow(this.props.links.prev)} >{this.props.labels[1]}</li>)
		} 
		return (<li id="prev" className="button nav disabled" >{this.props.labels[1]}</li>)
	}

	renderNextButton() {
		if (this.props.links.next) {
			return (<li id="next" className="button nav" onClick={() => this.props.follow(this.props.links.next)} >{this.props.labels[2]}</li>)
		} 
		return (<li id="next" className="button nav disabled" >{this.props.labels[2]}</li>)
	}

	renderLastButton() {
		if (this.props.links.next) {
			return (<li id="last" className="button nav" onClick={() => this.props.follow(this.props.links.last)} >{this.props.labels[3]}</li>)
		} 
		return (<li id="last" className="button nav disabled" >{this.props.labels[3]}</li>)
	}
}

