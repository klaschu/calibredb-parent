'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

import { BrowserRouter as Router, useLocation } from "react-router-dom";
import { Page, Paginator } from "./pagination";

function DefaultApp () {
  return (
	<Router>
		<QueryParamsWrapper />
	</Router>
  );
}

/**
 * Der Wraper ist notwendig, da die Verwendung von useQuery bzw. Router nicht in einer Component erlaubt ist.
 * @returns
 */
function QueryParamsWrapper() {
	  let query = new URLSearchParams(useLocation().search);
	  console.log("QueryParamsWrapper", query);
	  return (<div><Application size={query.get("size")} number={query.get("page")} /></div>);
}

class Application extends React.Component {

	constructor(props) {
		super(props);
		let size = (props.size ? props.size : 10);
		let number = (props.number ? props.number : 0);
		this.state = {books: [], page: {size: size, number: number}, links: {}};
		console.log("App constructor", props, this.state);
	}

	componentDidMount() { 
		console.log("App componentDidMount");
		client({method: 'GET', path: '/api/books', params: {size: this.state.page.size, page: this.state.page.number}}).done(response => {
			console.log(response.entity._embedded.page);
			this.setState({
				books: response.entity._embedded.book, 
				page: response.entity.page, 
				links: response.entity._links
			});
			console.log('State: ', this.state)
		});
	}
	
	follow(link) {
		console.log("App follow", this.state);
		client({method: 'GET', path: link.href, params: {size: this.state.page.size, page: this.state.page.number}}).done(response => {
			this.setState({
				books: response.entity._embedded.book, 
				page: response.entity.page, 
				links: response.entity._links
			});
		});
	}
	
	render() {
		console.log("App render");
		return (
			<div>
				<hr/>
				<Paginator links={this.state.links} follow={this.follow.bind(this)} labels={["First","Prev","Next","Last"]} />
				<hr/>
				<Page page = {this.state.page} />
				<hr/>
				<BookList books = {this.state.books} page = {this.state.page} />
			</div>
		)
	}
}

class BookList extends React.Component{
	render() {
		console.log("BookList render");
		const books = this.props.books.map(book => <Book key={book._links.self.href} book={book} />);
		return (
			<table>
				<thead>
					<tr>
						<th>Titel</th>
						<th>Autor</th>
					</tr>
				</thead>
				<tbody>
					{books}
				</tbody>
			</table>
		)
	}
}

class Book extends React.Component{
	render() {
		console.log("Book render");
		return (
			<tr>
				<td>{this.props.book.title}</td>
				<td>{this.props.book.authorSort}</td>
			</tr>
		)
	}
}

ReactDOM.render( DefaultApp(), document.getElementById('react') );