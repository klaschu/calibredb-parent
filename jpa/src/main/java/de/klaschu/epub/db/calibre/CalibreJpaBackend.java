package de.klaschu.epub.db.calibre;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import de.klaschu.epub.db.calibre.repositories.BookRepository;

@SpringBootApplication
public class CalibreJpaBackend {
	
    @Value("${server.port}") 
    private String serverPort;
	
    @Value("${spring.data.rest.base-path}") 
	private String apiPath; 
	
    @Value("${springfox.documentation.swagger.v2.path}") 
	private String apidocsPath; 

    private static final Logger log = LoggerFactory.getLogger(CalibreJpaBackend.class);
			
	public static void main(String[] args) {
		SpringApplication.run(CalibreJpaBackend.class, args);
	}

    @Bean
    CommandLineRunner initialize(BookRepository repository) {
        return args -> {
            log.info("REST Api: http://localhost:{}{}", serverPort, apiPath);
            log.info("Swagger Docs: http://localhost:{}{}", serverPort, apidocsPath);
            log.info("Swagger UI: http://localhost:{}/swagger-ui/index.html", serverPort);
        };
    }
    
}
