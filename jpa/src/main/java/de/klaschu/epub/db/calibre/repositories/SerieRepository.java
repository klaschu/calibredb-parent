package de.klaschu.epub.db.calibre.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import de.klaschu.epub.db.calibre.models.Serie;

/**
 * Siehe auch: <a href="https://spring.io/guides/gs/accessing-data-rest">accessing-data-rest</a>
 * @author Klaus
 *
 */
@RepositoryRestResource(collectionResourceRel = "serie", path = "series")
public interface SerieRepository extends PagingAndSortingRepository<Serie, Long> {
	
	List<Serie> findAll();
	Optional<Serie> findByName(@Param("name") String name);
	
	Page<Serie> findByNameLike(@Param("name") String name, Pageable pageable);
}
