package de.klaschu.epub.db.calibre.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import de.klaschu.epub.db.calibre.models.Tag;

/**
 * Siehe auch: <a href="https://spring.io/guides/gs/accessing-data-rest">accessing-data-rest</a>
 * @author Klaus
 *
 */
@RepositoryRestResource(collectionResourceRel = "tag", path = "tags")
public interface TagRepository extends PagingAndSortingRepository<Tag, Long> {
	
	List<Tag> findAll();
	Optional<Tag> findByName(@Param("name") String name);
	
	Page<Tag> findByNameLike(@Param("name") String name, Pageable pageable);
}
