package de.klaschu.epub.db.calibre.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "books_tags_link")
public class BooksTagsLink {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "book")
	private Long bookId;

	@Column(name = "tag")
	private Long tagId;
	
}
