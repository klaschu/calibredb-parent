package de.klaschu.epub.db.calibre.config;

import java.time.LocalDateTime;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

/**
 * Dokumentation siehe {@link LocalDateTimeStringJavaDescriptor}
 * 
 * @author Klaus
 */
public class LocalDateTimeStringType extends AbstractSingleColumnStandardBasicType<LocalDateTime> {

	private static final long serialVersionUID = -6793727898189283945L;
	public static final LocalDateTimeStringType INSTANCE = new LocalDateTimeStringType();

	public LocalDateTimeStringType() {
		super(VarcharTypeDescriptor.INSTANCE, LocalDateTimeStringJavaDescriptor.INSTANCE);
	}

	@Override
	public String getName() {
		return "LocalDateTimeString";
	}
}