package de.klaschu.epub.db.calibre.config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.ImmutableMutabilityPlan;

/**
 *	<p>Die in der Calibre DB als timestamp abgelegten Werte verursachen beim der Verarbeitung durch den SQLite JDBC Treiber ParseExceptions (siehe weiter unten).
 *	Die Klassen {@link LocalDateTimeStringJavaDescriptor} und {@link LocalDateTimeStringType} dienen dazu die entsprechnden Spalten als
 *  String zu lesen und nach LocalSDateTime zu konvertieren und umgekehrt. Eine in SQLite als timestamp definiert Spalte ist 
 *  in der Entity Bean wie folgt zu annotieren:</p>
 *  <pre>
 *	&#64;Column(name = "timestamp", columnDefinition = "TEXT")
 *	&#64;Type(type = "de.klaschu.epub.db.calibre.config.LocalDateTimeStringType")
 *	private LocalDateTime timestamp; </pre>
 * 	<p>Die Anweisung <code>columnDefinition = "TEXT"</code> sorg dafür, dass der JDBC Treiber die Spalte als Text ausliest und
 * 	so die Exception vermieden weird.</p>
 * 	Siehe <a href="https://www.baeldung.com/hibernate-custom-types">hibernate-custom-types</a>  
 * <p>Beispiel ParseException:</p><pre>
 * Caused by: java.text.ParseException: Unparseable date: "2015-10-23 07:05:51+00:00" does not match (\p{Nd}++)\Q-\E(\p{Nd}++)\Q-\E(\p{Nd}++)\Q \E(\p{Nd}++)\Q:\E(\p{Nd}++)\Q:\E(\p{Nd}++)\Q.\E(\p{Nd}++)
 *  at org.sqlite.date.FastDateParser.parse(FastDateParser.java:299) ~[sqlite-jdbc-3.25.2.jar:na]
 *  at org.sqlite.date.FastDateFormat.parse(FastDateFormat.java:490) ~[sqlite-jdbc-3.25.2.jar:na]
 *  at org.sqlite.jdbc3.JDBC3ResultSet.getTimestamp(JDBC3ResultSet.java:539) ~[sqlite-jdbc-3.25.2.jar:na]
 *  </pre>
 *  
 * @author Klaus
 *
 */
@SuppressWarnings("unchecked")
public class LocalDateTimeStringJavaDescriptor extends AbstractTypeDescriptor<LocalDateTime> {

	private static final long serialVersionUID = -7209554924383271208L;
	/**
	 * In der metadata.db von Calibre haben timestamp Spalten folgende Dateninhalte:
	 * 	2009-07-27 06:28:11.084000+00:00 mit sechs von 9 Ziffern von NanoOfSecond und Zeitzonenanpassung
	 * 	2011-05-18 00:00:00+00:00 ohne NanoOfSecond aber mit Zeitzonenanpassung
	 * 
	 */
	public static final DateTimeFormatter DATETIMEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss[.n]z");
	public static final LocalDateTimeStringJavaDescriptor INSTANCE = new LocalDateTimeStringJavaDescriptor();

	public LocalDateTimeStringJavaDescriptor() {
		super(LocalDateTime.class, ImmutableMutabilityPlan.INSTANCE);
	}

	@Override
	public LocalDateTime fromString(String datetimeString) {
		return LocalDateTime.parse(datetimeString, DATETIMEFORMATTER);
	}

	@Override
	public <X> X unwrap(LocalDateTime value, Class<X> type, WrapperOptions options) {
		if (value == null)
	        return null;

	    if (String.class.isAssignableFrom(type))
	        return (X) DATETIMEFORMATTER.format(value);

	    throw unknownUnwrap(type);
	}

	@Override
	public <X> LocalDateTime wrap(X value, WrapperOptions options) {
		if (value == null)
	        return null;

	    if(String.class.isInstance(value))
	        return LocalDateTime.from(DATETIMEFORMATTER.parse((CharSequence) value));

	    throw unknownWrap(value.getClass());
	}
}