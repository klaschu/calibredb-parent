package de.klaschu.epub.db.calibre.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Where;

@Entity(name = "identifiers")
@Where(clause = "type = 'isbn'")
public class Isbn {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "book")
	private Long book;

	@Column(name = "type")
	private String tpye;
	
	@Column(name = "val")
	private String value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBook() {
		return book;
	}

	public void setBook(Long book) {
		this.book = book;
	}

	public String getTpye() {
		return tpye;
	}

	public void setTpye(String tpye) {
		this.tpye = tpye;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Isbn [id=" + id + ", book=" + book + ", tpye=" + tpye + ", value=" + value + "]";
	}
}
