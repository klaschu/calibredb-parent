package de.klaschu.epub.db.calibre.models;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.klaschu.epub.db.calibre.config.LocalDateTimeStringJavaDescriptor;

@Entity(name = "books")
public class Book {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "title")
	private String title;

	@Column(name = "sort")
	private String sort;
	
	/**
	 * In der SQLite DB hat die Spalte den Type timestamp. 
	 * Leider kann der JDBC Treiber diesen Type nicht korrekt verarbeiten und wirft eine Exception.
	 * Siehe daher {@link LocalDateTimeStringJavaDescriptor} 
	 */
	@Column(name = "timestamp", columnDefinition = "TEXT")
	@Type(type = "de.klaschu.epub.db.calibre.config.LocalDateTimeStringType")
	private LocalDateTime timestamp;

	/**
	 * Siehe {@link #timestamp}
	 */
	@Column(name = "pubdate", columnDefinition = "TEXT")
	@Type(type = "de.klaschu.epub.db.calibre.config.LocalDateTimeStringType")
	private LocalDateTime pubdate;
	
	@Column(name = "series_index")
	private Double seriesIndex;
	
	@Column(name = "author_sort")
	private String authorSort;

	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "book")
//	@Filter(name = "isbnOnly")
	private Set<Isbn> isbns;
	
	@Column(name = "lccn")
	private String lccn;

	@Column(name = "path")
	private String path;

	@Column(name = "flags")
	private Integer flags;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "has_cover")
	private Boolean hasCover;
	
	/**
	 * Siehe {@link #timestamp}
	 */
	@Column(name = "last_modified", columnDefinition = "TEXT")
	@Type(type = "de.klaschu.epub.db.calibre.config.LocalDateTimeStringType")
	private LocalDateTime lastModified;

	/**
	 * Siehe auch: <a href="https://www.baeldung.com/the-persistence-layer-with-spring-and-jpa">the-persistence-layer-with-spring-and-jpa</a>
	 */
	@ManyToMany(mappedBy = "books", fetch = FetchType.EAGER)
	@JsonManagedReference 
    Set<Author> authors;

	@ManyToMany(mappedBy = "books", fetch = FetchType.EAGER)
	@JsonManagedReference 
    Set<Serie> series;

	@ManyToMany(mappedBy = "books", fetch = FetchType.EAGER)
	@JsonManagedReference 
    Set<Tag> tags;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime  datetime) {
		this.timestamp = datetime;
	}

	public LocalDateTime getPubdate() {
		return pubdate;
	}

	public void setPubdate(LocalDateTime datetime) {
		this.pubdate = datetime;
	}

	public Double getSeriesIndex() {
		return seriesIndex;
	}

	public void setSeriesIndex(Double seriesIndex) {
		this.seriesIndex = seriesIndex;
	}

	public String getAuthorSort() {
		return authorSort;
	}

	public void setAuthorSort(String authorSort) {
		this.authorSort = authorSort;
	}

	public String getIsbn() {
		return (isbns != null && isbns.iterator().hasNext()) ? 
				isbns.iterator().next().getValue()
				: null;
	}

	public Set<Isbn> getIsbns() {
		return isbns;
	}

	public void setIsbns(Set<Isbn> isbns) {
		this.isbns = isbns;
	}
	
	public String getLccn() {
		return lccn;
	}

	public void setLccn(String lccn) {
		this.lccn = lccn;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getFlags() {
		return flags;
	}

	public void setFlags(Integer flags) {
		this.flags = flags;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Boolean getHasCover() {
		return hasCover;
	}

	public void setHasCover(Boolean hasCover) {
		this.hasCover = hasCover;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime datetime) {
		this.lastModified = datetime;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Serie getSerie() {
		return series == null || series.isEmpty() ? null : series.iterator().next();
	}

	public Set<Serie> getSeries() {
		return series;
	}

	public void setSeries(Set<Serie> series) {
		this.series = series;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", sort=" + sort + ", timestamp=" + getTimestamp() + ", pubdate=" + getPubdate()
				+ ", seriesIndex=" + seriesIndex + ", authorSort=" + authorSort + ", isbn=" + getIsbn() + ", lccn=" + lccn + ", path=" + path
				+ ", flags=" + flags + ", uuid=" + uuid + ", hasCover=" + hasCover + ", lastModified=" + getLastModified() + "]";
	}

}
