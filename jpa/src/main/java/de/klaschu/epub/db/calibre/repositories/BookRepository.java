package de.klaschu.epub.db.calibre.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import de.klaschu.epub.db.calibre.models.Book;

@RepositoryRestResource(collectionResourceRel = "book", path = "books")
public interface BookRepository extends PagingAndSortingRepository<Book, Long> { 
	List<Book> findAll();
	Optional<Book> findByTitle(@Param("title") String title);
	
	/**
	 * 
	 * Siehe auch: <a href="https://www.baeldung.com/spring-jpa-like-queries">spring-jpa-like-queries</a>
	 * @param like
	 * @return
	 */
	Page<Book> findByTitleLike(@Param("name") String name, Pageable pageable);
	
	/**
	 * Die Suche funktioniert auch ohne die @Query da 'findByAuthorsSort' diese auch automatisch schon bildet.
	 *  Siehe: <a href="https://evonsdesigns.medium.com/spring-jpa-one-to-many-query-examples-281078bc457b">spring-jpa-one-to-many-query-examples</a>
	 * @param name
	 * @param pageable
	 * @return
	 */
	@Query( value = "select distinct b from books b inner join b.authors a where a.sort like :name")
	public Page<Book> findByAuthorsSortLike(@Param("name") String name, Pageable pageable);

	@Query( value = "select distinct b from books b inner join b.series s where s.name like :name")
	public Page<Book> findBySeriesNameLike(@Param("name") String name, Pageable pageable);

	@Query( value = "select distinct b from books b inner join b.tags t where t.name like :name")
	public Page<Book> findByTagsNameLike(@Param("name") String name, Pageable pageable);
}
