package de.klaschu.epub.db.calibre.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name = "tags")
public class Tag {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "books_tags_link", joinColumns = @JoinColumn(name = "tag"), inverseJoinColumns = @JoinColumn(name = "book"))
	@JsonBackReference
	Set<Book> books;

	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "book")
	Set<BooksTagsLink> bookLinks;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	public Integer getBookCount() {
		return (bookLinks == null) ? null : bookLinks.size();
	}
	
	@Override
	public String toString() {
		return "Serie [id=" + id + ", name=" + name + ", books=" + books + "]";
	}
	
}
