package de.klaschu.epub.db.calibre.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.JoinColumn;

@Entity(name = "authors")
public class Author {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;

	@Column(name = "sort")
	private String sort;

	/**
	 * Siehe auch: <a href="https://www.baeldung.com/the-persistence-layer-with-spring-and-jpa">the-persistence-layer-with-spring-and-jpa</a>
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "books_authors_link", joinColumns = @JoinColumn(name = "author"), inverseJoinColumns = @JoinColumn(name = "book"))
	@JsonBackReference
	Set<Book> books;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	public Integer getBookCount() {
		return (books == null) ? null : books.size();
	}
	
	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", sort=" + sort + "]";
	}
	
}
