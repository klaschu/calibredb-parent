package de.klaschu.epub.db.calibre.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import de.klaschu.epub.db.calibre.models.Author;

/**
 * Siehe auch: <a href="https://spring.io/guides/gs/accessing-data-rest">accessing-data-rest</a>
 * @author Klaus
 *
 */
@RepositoryRestResource(collectionResourceRel = "author", path = "authors")
public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
	
	List<Author> findAll();
	Optional<Author> findByName(@Param("name") String name);
	
	/**
	 * 
	 * Siehe auch: <a href="https://www.baeldung.com/spring-jpa-like-queries">spring-jpa-like-queries</a>
	 * @param nameStart
	 * @return
	 */
	Page<Author> findByNameStartsWith(@Param("name") String nameStart, Pageable pageable);
	Page<Author> findByNameLike(@Param("name") String name, Pageable pageable);
}
