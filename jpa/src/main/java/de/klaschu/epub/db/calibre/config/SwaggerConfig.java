package de.klaschu.epub.db.calibre.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Server;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
    public Docket api() {
        Server serverLocal = new Server("local", "http://localhost:8080", "for local usages", Collections.emptyList(), Collections.emptyList());
        Server serverRavensteyn = new Server("local", "https://ravensteyn.de/springboot", "for local usages", Collections.emptyList(), Collections.emptyList());
        return new Docket(DocumentationType.SWAGGER_2)
        		.servers(serverLocal, serverRavensteyn)
        		.apiInfo(apiInfo())
        		.select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/(api|util)/.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger API Documentation Mit SpringFox")
                .description("REST API zur Calibre Buch DB")
                .version("1.0.0")
                .contact(new Contact("Klaus", "https://ravensteyn.de", "klaus@ravensteyn.de"))
                .build();
    }

}