package de.klaschu.epub.db.calibre.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "books_series_link")
public class BooksSeriesLink {

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "book")
	private Long bookId;

	@Column(name = "series")
	private Long serieId;
	
}
