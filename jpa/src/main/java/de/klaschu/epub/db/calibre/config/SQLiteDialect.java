package de.klaschu.epub.db.calibre.config;

import java.sql.Types;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.identity.IdentityColumnSupport;

public class SQLiteDialect extends Dialect {
	enum SQLiteTypes {	integer, tinyint, smallint, bigint, real, numeric, decimal, varchar, longvarchar, date, time, timestamp, blob, clob };    
    public SQLiteDialect() {
        registerColumnType(Types.BIT, SQLiteTypes.integer.name());
        registerColumnType(Types.TINYINT, SQLiteTypes.tinyint.name());
        registerColumnType(Types.SMALLINT, SQLiteTypes.smallint.name());
        registerColumnType(Types.INTEGER, SQLiteTypes.integer.name());
        registerColumnType(Types.BIGINT, SQLiteTypes.bigint.name());
        registerColumnType(Types.FLOAT, "float");
        registerColumnType(Types.REAL, SQLiteTypes.real.name());
        registerColumnType(Types.DOUBLE, "double");
        registerColumnType(Types.NUMERIC, SQLiteTypes.numeric.name());
        registerColumnType(Types.DECIMAL, SQLiteTypes.decimal.name());
        registerColumnType(Types.CHAR, "char");
        registerColumnType(Types.VARCHAR, SQLiteTypes.varchar.name());
        registerColumnType(Types.LONGVARCHAR, SQLiteTypes.longvarchar.name());
        registerColumnType(Types.DATE, SQLiteTypes.date.name());
        registerColumnType(Types.TIME, SQLiteTypes.time.name());
        registerColumnType(Types.TIMESTAMP, SQLiteTypes.timestamp.name());
        registerColumnType(Types.BINARY, SQLiteTypes.blob.name());
        registerColumnType(Types.VARBINARY, SQLiteTypes.blob.name());
        registerColumnType(Types.LONGVARBINARY, SQLiteTypes.blob.name());
        registerColumnType(Types.BLOB, SQLiteTypes.blob.name());
        registerColumnType(Types.CLOB, SQLiteTypes.clob.name());
        registerColumnType(Types.BOOLEAN, SQLiteTypes.integer.name());
    }

    @Override
    public IdentityColumnSupport getIdentityColumnSupport() {
        return new SQLiteIdentityColumnSupport();
    }

    @Override
    public boolean hasAlterTable() {
        return false;
    }

    @Override
    public boolean dropConstraints() {
        return false;
    }

    @Override
    public String getDropForeignKeyString() {
        return "";
    }

    @Override
    public String getAddForeignKeyConstraintString(String constraintName, String[] foreignKey, String referencedTable, String[] primaryKey, boolean referencesPrimaryKey) {
        return "";
    }

    @Override
    public String getAddPrimaryKeyConstraintString(String constraintName) {
        return "";
    }

    @Override
    public String getForUpdateString() {
        return "";
    }

    @Override
    public String getAddColumnString() {
        return "add column";
    }

    @Override
    public boolean supportsOuterJoinForUpdate() {
        return false;
    }

    @Override
    public boolean supportsIfExistsBeforeTableName() {
        return true;
    }

    @Override
    public boolean supportsCascadeDelete() {
        return false;
    }
}
