package de.klaschu.epub.db.calibre;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import de.klaschu.epub.db.calibre.models.Author;
import de.klaschu.epub.db.calibre.models.Book;
import de.klaschu.epub.db.calibre.repositories.AuthorRepository;
import de.klaschu.epub.db.calibre.repositories.BookRepository;

@DataJpaTest 
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CalibreJpaBackendTests {

    @Autowired
    private BookRepository bookRepository;
    
    @Autowired
    private AuthorRepository authorRepository;
    
    @Test
	void contextLoads() {
    	assertNotNull(bookRepository);
    	assertNotNull(authorRepository);
	}
    
    @Test
    void GetAllBooks() {
    	List<Book> books = bookRepository.findAll();
    	assertEquals(3995, books.size());
    	books.forEach(System.out::println);
    }
	
    @Test
    void GetAllAuthors() {
    	List<Author> authors = authorRepository.findAll();
    	assertEquals(2239, authors.size());
    	authors.forEach(System.out::println);
    }
    
    @Test
    void getAuthorById() {
    	Optional<Author> optional = authorRepository.findById(933L);
    	System.out.println(optional);
    	for (Book book : optional.get().getBooks()) {
        	System.out.println(book);
    	}
    	assertEquals(7, optional.get().getBooks().size());
    }
    
    @Test
    void findAuthorsLike() {
    	Pageable pagable = PageRequest.of(0, 50000);
    	Page<Author> authors = authorRepository.findByNameLike("%Börjlind%", pagable);
    	assertEquals(3, authors.getContent().size());
    	authors.getContent().forEach(System.out::println);
    }

    @Test
    void findAuthorsStartWith() {
    	Pageable pagable = PageRequest.of(0, 50000);
    	Page<Author> authors = authorRepository.findByNameStartsWith("Börjlind", pagable);
    	assertEquals(3, authors.getContent().size());
    	authors.getContent().forEach(System.out::println);
    }

    @Test
    void findBookByTitle() {
    	/*
    	 * id: 1755
    	 * title: Die Insel, die Kolumbus nicht gefunden hat: Sieben Gesichter Japans		
    	 * sort: Die Insel, die Kolumbus nicht gefunden hat: Sieben Gesichter Japans	
    	 * timestamp: 2015-10-16 09:02:59.506000+00:00	
    	 * pubdate: 1995-02-14 23:00:00+00:00	
    	 * series_index: 1	
    	 * author_sort:	Muschg, Adolf
    	 * isbn:	
    	 * lccn:	
    	 * path: Muschg, Adolf/Die Insel, die Kolumbus nicht gefun (1755)
    	 * flags: 1
    	 * uuid: 7d79cc3e-41d6-423c-aca8-5d1de82f4cd6
    	 * has_cover: 1
    	 * last_modified: 2021-01-12T10:45:48.000005650
    	 * 	    	 
    	 */
    	String title = "Die Insel, die Kolumbus nicht gefunden hat: Sieben Gesichter Japans";
    	Optional<Book> optional = bookRepository.findByTitle(title);
    	System.out.println(optional);
    	for (Author author : optional.get().getAuthors()) {
        	System.out.println(author);
    	}
    	assertEquals(true, optional.isPresent());
    	assertEquals(1755, optional.get().getId());
    	assertEquals(title, optional.get().getTitle());
    	assertEquals(title, optional.get().getSort());
    	assertEquals(LocalDateTime.of(2015,10,16,9,2,59,506000), optional.get().getTimestamp());
    	assertEquals(LocalDateTime.of(1995,2,14,23,00), optional.get().getPubdate());
    	assertEquals(1, optional.get().getSeriesIndex());
    	assertEquals("Muschg, Adolf/Die Insel, die Kolumbus nicht gefun (1755)", optional.get().getPath());
    	assertEquals(1, optional.get().getFlags());
    	assertEquals("7d79cc3e-41d6-423c-aca8-5d1de82f4cd6", optional.get().getUuid());
    	assertEquals(true, optional.get().getHasCover());
    	assertEquals(LocalDateTime.of(2021,1,12,10,45,48,5650), optional.get().getLastModified());
    	assertEquals(1, optional.get().getAuthors().size());
    }

}
