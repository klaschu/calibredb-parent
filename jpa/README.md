Module zu Calibre DB - Datenbankzugriff mit JPA bereitgestellt als REST Api   
=========

Die Daten von Calibre liegen in einer [SQLite](https://www.sqlite.org/index.html) Datenbank 'metadata.db' vor.
Um sie über JPA/Hibernate zuzugreifen waren eine paar Besonderheite notwendig, die auch bei anderen nicht Standard Datenbanken interessant sein könnten:

- In der Klasse SQLiteDialect wird, unter anderem, festgelegt, welche Spalten Typen SQLLite kennt und auf welche Java Klassen sie zu mappen sind.
- Die Klasse SQLiteIdentityColumnSupport legt die Behandlung von @Id Spalten fest.
- Die in der Calibre DB als timestamp abgelegten Werte verursachen beim der Verarbeitung durch den SQLite JDBC Treiber ParseExceptions.  
Die Klassen LocalDateTimeStringJavaDescriptor und LocalDateTimeStringType dienen dazu die entsprechenden Spalten als String zu lesen und nach LocalSDateTime zu konvertieren und umgekehrt.       


Die Daten werden als [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS) Api bereit gestellt.

