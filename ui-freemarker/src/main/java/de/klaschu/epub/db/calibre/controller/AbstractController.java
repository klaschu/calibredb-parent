package de.klaschu.epub.db.calibre.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Controller
public abstract class AbstractController {
	private static final Logger log = LoggerFactory.getLogger(AbstractController.class);
	protected static final String BASE_URI = "/ui/fm/";
	
	@Value("${paginator.segment.size}")
	private Integer segmentSize;  

    @Value("${ui.freemarker.base-path}") 
	private String basePath;
	
	protected String fillModel(Model model, Pageable pageable, String search) {
		log.debug("index pageable: {}, search: {}", pageable, search);
		
		Page<?> bookPage = search(pageable, search); 
		Paginator paginator = Paginator.create(bookPage, segmentSize, BASE_URI + getPagename() + "?page=%s" + (StringUtils.hasText(search) ? "&search=" + search : ""));
		
		model.addAttribute("basepath", basePath);
		model.addAttribute("title", getTitle());
		model.addAttribute("paginator", paginator);
		model.addAttribute("pagename", getPagename());
		model.addAttribute("page", bookPage);
		model.addAttribute("search", search);

		if (log.isDebugEnabled()) {
			try {
				log.debug("Model: {}", new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).writeValueAsString(model));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return getPagename();
	}

	protected abstract Page<?> search(Pageable pageable, String search);
	
	protected abstract String getPagename();
	protected abstract String getTitle();
	
	protected String likeSearchValue(String searchValue) {
		return String.format("%%%s%%", searchValue);
	}

}