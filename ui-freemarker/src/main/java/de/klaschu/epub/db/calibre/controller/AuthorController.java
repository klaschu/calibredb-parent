package de.klaschu.epub.db.calibre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.klaschu.epub.db.calibre.repositories.AuthorRepository;

@Controller
public class AuthorController extends AbstractController {
	private static final String PAGE_NAME = "authors";
	
	@Autowired
	private AuthorRepository authorRepository;

	@GetMapping(value = BASE_URI + PAGE_NAME)
	public String books(Model model, @PageableDefault(size = 15) Pageable pageable, @RequestParam(value = "search", required = false) String search ) {
		return fillModel(model, pageable, search);
	}

	@Override
	protected Page<?> search(Pageable pageable, String search) {
		if (StringUtils.hasText(search)) {
			return authorRepository.findByNameLike(likeSearchValue(search), pageable);
		}
		return authorRepository.findAll(pageable);
	}

	@Override
	protected String getPagename() {
		return PAGE_NAME;
	}

	@Override
	protected String getTitle() {
		return "Authors (freemarker)";
	}
}