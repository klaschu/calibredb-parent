package de.klaschu.epub.db.calibre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalibreFreemarkerUiApp {
			
	public static void main(String[] args) {
		SpringApplication.run(CalibreFreemarkerUiApp.class, args);
	}
}
