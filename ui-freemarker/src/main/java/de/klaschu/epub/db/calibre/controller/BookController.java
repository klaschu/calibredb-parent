package de.klaschu.epub.db.calibre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.klaschu.epub.db.calibre.repositories.BookRepository;

@Controller
public class BookController extends AbstractController {
	private static final String PAGE_NAME = "books";
	private enum SearchField {title, author, serie, tag}
	
	@Autowired
	private BookRepository bookRepository;

	@GetMapping(value = BASE_URI + PAGE_NAME)
	public String books(Model model, @PageableDefault(sort = {"authorSort" }, size = 15) Pageable pageable, @RequestParam(value = "search", required = false) String search) {
		return fillModel(model, pageable, search);
	}

	@Override
	protected Page<?> search(Pageable pageable, String search) {
		String searchValue = null;
		if (StringUtils.hasText(search)) {
			SearchField searchField = null;
			int columnIndex = search.indexOf(":");
			if (columnIndex > 0) {
				searchField = SearchField.valueOf(search.substring(0, columnIndex));
				searchValue = search.substring(columnIndex + 1);
			} else {
				searchField = SearchField.title;
				searchValue = search;
			}
			if (SearchField.title.equals(searchField)) {
				return bookRepository.findByTitleLike(likeSearchValue(searchValue), pageable);
			}
			if (SearchField.author.equals(searchField)) {
				return bookRepository.findByAuthorsSortLike(likeSearchValue(searchValue), pageable);
			}
			if (SearchField.serie.equals(searchField)) {
				return bookRepository.findBySeriesNameLike(likeSearchValue(searchValue), pageable);
			}
			if (SearchField.tag.equals(searchField)) {
				return bookRepository.findByTagsNameLike(likeSearchValue(searchValue), pageable);
			}
		}
		return bookRepository.findAll(pageable);
	}

	@Override
	protected String getPagename() {
		return PAGE_NAME;
	}

	@Override
	protected String getTitle() {
		return "Books (freemarker)";
	}
}