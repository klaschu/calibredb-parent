package de.klaschu.epub.db.calibre.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

public class Paginator {
	private static final Logger log = LoggerFactory.getLogger(Paginator.class);
	private List<PageItem> items = new ArrayList<Paginator.PageItem>();
	private int currentPage;
	private int totalPages;
	private Long totalElements = -1L;
	private int partLength;
	private String linkPattern;
	
	public Paginator(int currentPage, int totalPages, Long totalElements, int partLength, String linkPattern) {
		super();
		log.debug("Paginator currentPage {}, totalPages {}, totalElements {}, partLength {}, linkPattern {}", currentPage, totalPages, totalElements, partLength, linkPattern );
		this.currentPage = currentPage;
		this.totalPages = totalPages;
		this.partLength = partLength;
		this.linkPattern = linkPattern;
		if (totalElements != null) {
			this.totalElements = totalElements;
		}
	}

	public static Paginator create(Page<?> page, int partLength, String linkPattern) {
		Paginator instamce = new Paginator(page.getNumber(), page.getTotalPages(), page.getTotalElements(), partLength, linkPattern);
		instamce.createItems();
		return instamce;
	}

	public static Paginator create(int currentPage, int totalPages, Long totalElements, int partLength, String linkPattern) {
		Paginator instamce = new Paginator(currentPage, totalPages, totalElements, partLength, linkPattern);
		instamce.createItems();
		return instamce;
	}
	
	public List<PageItem> getItems() {
		return items;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public int getPartLength() {
		return partLength;
	}

	private void createItems() {
		if (totalPages == 1) {
			int page = currentPage;
			String cssClass = String.format("page-item %s", currentPage == page ? " active" : "");
			items.add(new PageItem(items.size(), String.valueOf(page), cssClass, page, linkPattern));
			return;
		}
		
		items.add(new PageItem(items.size(), "<", String.format("page-item page-pre%s", currentPage > 0 ? "" : " disabled"), currentPage - 1, linkPattern));
//		log.debug("createNavItems CurrentPage: {}, PartLength: {}, TotalPages: {}", currentPage, partLength, totalPages);
		
		int firstColons = calcFirstColons();
		int lastColons = calcLastColons();
		for (int part = 0; part < 3; part++) {

			if (part == 1) {
				for (int i = 0; i < firstColons; i++) {
//					log.debug("-item page-first-separator nr {}", i);
					items.add(new PageItem(items.size(), "...","page-item page-first-separator disabled", -1, linkPattern));
				}
			}

			int last = calcLast(part, partLength, currentPage, totalPages);
			int first = 1 + last - partLength;
//			log.debug("Part: {}, First: {}, Last: {}, ", part, first, last);
			if (last > 0) {
				for (int page = first; page <= last; page++) {
					String cssClass = String.format("page-item %s", currentPage == page ? " active" : "");
					items.add(new PageItem(items.size(), String.valueOf(page), cssClass, page, linkPattern));
				}

				if (part == 1) {	
					for (int i = 0; i < lastColons; i++) {
//						log.debug("-item page-last-separator nr {}", i);
						items.add(new PageItem(items.size(), "...","page-item page-last-separator disabled", -1, linkPattern));
					}
				}
			}
		}
		
		items.add(new PageItem(items.size(), ">", String.format("page-item page-next%s", currentPage + 1 < totalPages ? "" : " disabled"), currentPage + 1, linkPattern));
	}

	private int calcFirstColons() {
		if (currentPage < partLength * 2) {
			return 0;
		}
		if (currentPage >= partLength * 2 && currentPage < totalPages - partLength * 2) {
			return 1;
		}
		return 2;
	}

	private int calcLastColons() {
		if (currentPage < partLength * 2) {
			return 2;
		}
		if (currentPage >= partLength * 2 && currentPage < totalPages - partLength * 2) {
			return 1;
		}
		return 0;
	}

	private int calcLast(int part, int partLength, int currentPage, int totalPages) {
		if (part == 0) {
			return partLength - 1;
		}
		if (part == 2) {
			return totalPages - 1;
		}
		int firstOfLastPart = totalPages - partLength - 1;
		if (currentPage > firstOfLastPart - partLength) {
			return firstOfLastPart;
		}
		if (currentPage < partLength * 2) {
			return partLength * 2 - 1;
		}
		return currentPage + (partLength / 2);
	}
	
	
	public static class PageItem {
		Integer id;
		String title;
		String cssClass;
		Integer page;
		String link;
		
		public PageItem(Integer id, String title, String cssClass, Integer page, String linkPattern) {
			super();
			this.id = id;
			this.title = title;
			this.cssClass = cssClass;
			this.page = cssClass.contains("disabled") ? - 1 : page;
			this.link = String.format(linkPattern, page);
		}

		public Integer getId() {
			return id;
		}

		public String getTitle() {
			return title;
		}

		public String getCssClass() {
			return cssClass;
		}

		public Integer getPage() {
			return page;
		}

		public String getLink() {
			return link;
		}
		@Override
		public String toString() {
			return "NavItem [id=" + id + ", title=" + title + ", cssClass=" + cssClass + ", page=" + page + ", link=" + link + "]";
		}
		
	}
}
