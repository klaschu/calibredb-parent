Module zu Calibre DB - Web Frontend mit Freemarker Template   
=========

Dieses Module stellt ein Web Frontend zur Calibre DB mit [Spring Boot MVC](https://spring.io/guides/gs/serving-web-content/)
und verwendet dabei als Template Engine [Freemarker](https://freemarker.apache.org/).

Die Anzeige wird komplett serverseitig erzeugt.

Die Daten werden über die REST Schnittstelle des Modul 'Calibre DB mit JPA' abgerufen.

