Module zu Calibre DB - Web Frontend App mit Welcome Seite   
=========

Dieses Module stellt die Applikation **CalibreApplication**, ein Web Frontend mit [Spring Boot MVC](https://spring.io/guides/gs/serving-web-content/)
und Welcome Seite zur Verfügung. Die Welcome Seite verlinkt zu der Ausgabe der weiteren Module.   

### Module

- calibredb-app (dieses Module)
- calibredb-jpa
- calibredb-ui-freemarker
- calibredb-ui-react

 
