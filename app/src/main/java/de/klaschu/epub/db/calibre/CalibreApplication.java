package de.klaschu.epub.db.calibre;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalibreApplication {
	
	private static final Logger log = LoggerFactory.getLogger(CalibreApplication.class);
			
	public static void main(String[] args) {
		log.info("Starte {}", CalibreApplication.class.getSimpleName());
		SpringApplication.run(CalibreApplication.class, args);
	}
}
