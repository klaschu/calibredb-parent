Module zu Calibre DB - Web Frontend mit React   
=========

Dieses Module stellt ein Web Frontend zur Calibre DB mit [React](https://reactjs.org/) zur Verfügung.
Dabei wird der wesentliche Teil der Anzeige über Javascript aus Komponenten erzeugt. 
Die Daten werden über die REST Schnittstelle des Modul 'Calibre DB mit JPA' abgerufen.

Besondere Herausforderung für den Java Entwickler, Verwendung des JS Workflow mit

- Node.js [siehe](https://nodejs.org/de/about/)
- npm [siehe](https://docs.npmjs.com/about-npm)
- webpack [siehe](https://webpack.js.org)

Die Integration wird heir mit dem Maven Plugin [frontend-maven-plugin](https://repo1.maven.org/maven2/com/github/eirslett/frontend-maven-plugin/) gelöst.

## Building 

##### Build and run tests:

```
$ mvn clean install
```

##### Install node.js and mnp - Installiert node.js und npm im Ordner 'node' des Modul. 

```
$ mvn frontend:install-node-and-npm
```

Alternativ, falls node.sj und npm schon auf dem Rechner installiert sind, ist dem frontend-maven-plugin mitzuteilen, wo node.js zu finden ist.

```
			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<!-- Use the latest released version: https://repo1.maven.org/maven2/com/github/eirslett/frontend-maven-plugin/ -->
				<version>1.9.1</version>
				<configuration>
					<nodeVersion>v15.5.1</nodeVersion>
					<npmVersion>6.14.11</npmVersion>
   					<installDirectory>C:\Program Files\</installDirectory>
				</configuration>
			</plugin>
```
**Achtung:** 
Der Installationspfad unter Windows für node.js lautet 'C:\Program Files\nodejs\node.exe'.
Das Plugin ergänzt installDirectory um '\node\node.exe'. Das passt dann leider nicht.

	Cannot run program "C:\Program Files\node\node.exe"
	
Abhilfe kann hier ein symbolischer Link leisten:

	mklink /D "c:\Program Files\node" "c:\Program Files\nodejs"
	
Oder
Die installDirectory Anweisung löschen 
und mit 'mvn frontend:install-node-and-npm' Node im Projektordner installieren.		     						 
  

##### Run npm - Installiert die in 'package.json' festgelegten mit allen benötigten JS Paketen im Modul Ordner 'node_modules'  

```
$ mvn frontend:npm
```
##### Run Webpack - Erstellt aus den Javascript Dateien in 'src/main/js' ausgehend von 'books.js' eine Bundledatei 'src/main/resources/static/react/books/build/bundle.js' 
in der alle Javascript benötigten Dateien enthalten sind. 

Achtung: Danach Refresh auf den Projekt Ordner ausführen, da Eclipse die Änderungen sonst nicht mit bekommt.   

```
$ mvn frontend:webpack
```


