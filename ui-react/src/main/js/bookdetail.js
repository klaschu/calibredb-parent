import React from "react";
const client = require('./client');

export class BookDetails extends React.Component {
	constructor(props) {
		  super(props);
		  this.state = {};
//		  this.handleClick = this.handleClick.bind(this);
		console.log('BookDetails constructor', props, this.state)
	}
	
	componentDidMount() {
		console.log('BookDetails componentDidMount')
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('BookDetails componentDidUpdate', prevProps, prevState, snapshot)
	}
	
	componentWillUnmount() {
		console.log('BookDetails componentWillUnmount')
	}
	
	render() {
		console.log('BookDetails render')
//		console.log('BookDetails render', this.props.book)
		const details = this.props.books.map(book => <BookDetail key={book._links.self.href} book={book} search = {this.props.search} />); 
		return (
			<div>
				{details}
			</div>	
		)
	}
}

export class BookDetail extends React.Component {
	constructor(props) {
		  super(props);
		  this.state = {};
//		  this.handleClick = this.handleClick.bind(this);
		console.log('BookDetail constructor', props, this.state)
	}
	
	componentDidMount() {
		console.log('BookDetail componentDidMount')
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('BookDetail componentDidUpdate', prevProps, prevState, snapshot)
	}
	
	componentWillUnmount() {
		console.log('BookDetail componentWillUnmount')
	}

	render() {
//		console.log('BookDetail render')
		console.log('BookDetail render', this.props.book)
		const fields = [{label:'Id',name:'id'}, {label:'Title',name:'title'}, {label:'ISBN',name:'isbn'}, {label:'Published',name:'pubdate'}]
		if (this.props.book.id) {
			return (
				<div  id={this.props.book.id} className="detail" >
					<table id="detailTable" className="table caption-top w-autox bookdetail" >
						<thead>
							<tr><th colSpan="3" className="text-center">Book Detail</th></tr>
						</thead>
						<tbody>
							{fields.map(field => {
								return (
										<tr key={field.name}><td>{field.label}</td><td>{this.props.book[field.name]}</td></tr>	
								);
							})}
							<tr><td>Serie</td><BookSerie book = {this.props.book} search = {this.props.search} /></tr>
							<tr><td>Tags</td><BookTags book = {this.props.book} search = {this.props.search} /></tr>
						</tbody>
					</table>
					<BookAuthors book = {this.props.book} search = {this.props.search} />
				</div>
			)
		}
		return (
				<div className="detail" >
				<table id="detailTable" className="table caption-top w-autox bookdetail" >
					<thead>
						<tr><th colSpan="3" className="text-center">Book x Detail</th></tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		)
	}
}

export class BookSerie extends React.Component{

	constructor(props) {
		super(props);
	    this.state = {value: ''};
	    this.handleClick = this.handleClick.bind(this);
		console.log('BookSerie constructor', props, this.state)
	}

	
	componentDidMount() {
		console.log('BookSerie componentDidMount')
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('BookSerie componentDidUpdate', prevProps, prevState, snapshot)
	}
	
	componentWillUnmount() {
		console.log('BookSerie componentWillUnmount')
	}
	
	handleClick(event) {
		console.log('BookSerie handleClick', event);
		this.props.search(this.props.book.serie.name, 'serie');
	    event.preventDefault();
	}
	  
	render() {
		console.log("BookSeries render", this.props.book.serie);
		if (this.props.book.serie) { 
			return (
				<td>
					<a onClick={this.handleClick} href="" >{this.props.book.serie.name}</a> Band {this.props.book.seriesIndex}
				</td>
			)
		}
		return (<td></td>)
	}
}

export class BookTags extends React.Component{
	constructor(props) {
		  super(props);
		  this.state = {tags: []};
		  this.handleClick = this.handleClick.bind(this);
		console.log('BookTags constructor', props, this.state)
	}

	loadData() {
		client({method: 'GET', path: this.props.book._links.tags.href}).done(response => {
			console.log(response.entity);
			this.setState({
				tags: response.entity._embedded.tag
			});
			console.log('BookTags GET State: ', this.state)
		});
	}
	
	componentDidMount() {
		console.log('BookTags componentDidMount', this.props.book)
		this.loadData();
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('BookTags componentDidUpdate revState', prevState, 'prevProps', prevProps)
		console.log('BookTags componentDidUpdate State', this.state, 'Props', this.props)
		if (prevProps.book.id != this.props.book.id) {
			this.loadData();
			console.log('BookTags fresh loaded', prevState)
		} else {
			console.log('BookTags already loaded', prevState)
		}
	}
	
	componentWillUnmount() {
		console.log('BookTags componentWillUnmount')
	}

	handleClick(event, tag) {
		console.log('BookTag handleClick', event, tag);
		this.props.search(tag, 'tag');
	    event.preventDefault();
	}
	  
	render() {
//		console.log("BookTags render");
		console.log("BookTags render", this.props.book._links.tags);
		const tags = this.state.tags.map((tag,index)  => <span key={index} ><a href="" onClick={(e) => this.handleClick(e,tag.name)} >{tag.name}</a>&nbsp;</span>);
		if (this.state.tags) {
			return (
					<td>
						{tags}
					</td>	
				)
		}
		return (<td></td>)
	}
}

export class BookAuthors extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			link: this.props.book._links.authors.href,
			authors: []
		};
		console.log('BookAuthors constructor', props, this.state)
	}
	
	loadData() {
		client({method: 'GET', path: this.props.book._links.authors.href}).done(response => {
			console.log(response.entity);
			this.setState({
				authors: response.entity._embedded.author
			});
			console.log('BookAuthors GET State: ', this.state)
		});
	}
	
	componentDidMount() {
		console.log('BookAuthors componentDidMount', this.state)
		this.loadData();
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('BookAuthors componentDidUpdate revState', prevState, 'prevProps', prevProps)
		console.log('BookAuthors componentDidUpdate State', this.state, 'Props', this.props)
		if (prevProps.book.id != this.props.book.id) {
			this.loadData();
			console.log('BookAuthors fresh loaded', prevState)
		} else {
			console.log('BookAuthors already loaded', prevState)
		}
	}
	
	componentWillUnmount() {
		console.log('BookAuthors componentWillUnmount')
	}

	render() {
//		console.log("BookAuthors render");
		console.log("BookAuthors render", this.state);
		if (this.state.authors) {
			const authors = this.state.authors.map((author, index) => <Author key = {index} author = {author} search = {this.props.search} />);
			return (
				<table className="table table-striped table-hover caption-top">
					<caption>Authors</caption>
					<thead>
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Name</th>
						<th scope="col"># Books</th>
					</tr>
					</thead>
					<tbody>
						{authors}
					</tbody>
				</table>
			)
		}
		return (
			<div></div>	
		)
	}
}	

export class Author extends React.Component{

	constructor(props) {
		super(props);
		this.state = {};
		this.handleClick = this.handleClick.bind(this);
		console.log('Author constructor', props, this.state)
	}
	
	componentDidMount() {
		console.log('Author componentDidMount')
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('Author componentDidUpdate', prevProps, prevState, snapshot)
	}
	
	componentWillUnmount() {
		console.log('Author componentWillUnmount')
	}

	handleClick(event, author) {
		console.log('BookSerie handleClick', event, author);
		this.props.search(author, 'author');
	    event.preventDefault();
	}
	  
	render() {
		console.log("Author render");
//		console.log("Author render", this.props.auhtor);
		return (
			<tr>
				<td scope="row">{this.props.author.id}</td>
				<td scope="row"><a onClick={(e) => this.handleClick(e,this.props.author.name)} href="">{this.props.author.name}</a></td>
				<td scope="row">{this.props.author.bookCount}</td>
			</tr>
		)
	}
}
