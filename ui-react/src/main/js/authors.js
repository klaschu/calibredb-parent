'use strict';
const React = require('react');
const client = require('./client');

import { Helmet } from 'react-helmet';
import { Navbar, Paginator } from "./utils";
import { MasterDetailPage } from "./masterdetail";

export class AuthorsPage extends MasterDetailPage {
	
	constructor(props) {
		super(props);
		console.log("AuthorsPage constructor", props, this.state);
	}
	
//	render() {
//		console.log("AuthorPage render", this.state);
////		const authors = this.props.authors.map(author => <Author key={author.id} author = {author} search = {this.props.search} />)
//		const authors = this.state.records.map(author => <Author key={author.id} author = {author} showdetail = {this.showdetail.bind(this)} search = {this.search.bind(this)} />)
//		return (
//			<div>
//				<Helmet><title>Author (react)</title></Helmet>
//				<Navbar/>
//				<Paginator page = {this.state.page} gotopage = {this.gotopage.bind(this)} />	
//				<div className="row align-items-start">
//					<div className="col">
//						<table id="mainTable" className="table table-striped table-hover caption-top">
//							<thead className="bg-gradient-info">
//								<tr>
//									<th scope="col">Id</th>
//									<th scope="col">Name</th>
//									<th scope="col"># Books</th>
//								</tr>
//							</thead>
//							<tbody>
//								{authors}
//							</tbody>
//						</table>
//					</div>
//					<div className="col"> 
//						
//					</div>
//				</div>
//			</div>
//		)
//	}
}

class Author extends React.Component{

	handleClick(event, author) {
		console.log('BookSerie handleClick', event, author);
		this.props.search(author, 'author');
	    event.preventDefault();
	}

	render() {
		console.log("Author render");
		return (
			<tr data-id={this.props.author.id} onClick={(e) => this.props.showdetail(e, this.props.author.id)} >
				<td scope="row">{this.props.author.id}</td>
				<td scope="row"><a onClick={(e) => this.handleClick(e,this.props.author.name)} href="">{this.props.author.name}</a></td>
				<td scope="row">{this.props.author.bookCount}</td>
			</tr>
		)
	}
}

/*
	<div class="container">
		<@util.navbar searchUrl='/fm/authors' searchValue=search!''/>
		<@util.paginator paginatorObject=paginator />
		<div class="row align-items-start">
			<div class="col">
				<table id="mainTable" class="table table-striped table-hover caption-top">
					<thead class="bg-gradient-info">
						<tr>
							<th scope="col">Id</th>
							<th scope="col">Name</th>
							<th scope="col"># Books</th>
					</tr>
					</thead>
					<tbody>
						<#list page.content as author>
						<#assign authorbooks = author.books?size>
						<tr data-id="${author.id?c}">
							<td scope="row">${author.id?c}</td>
							<td scope="row">${author.name}</td>
							<td scope="row">
								<#if authorbooks < 1>
									&nbsp;
								<#else>
									<a href="/fm/books?page=0&search=author:${author.name}">${authorbooks?c}</a>
								</#if>	
							</td>
						</tr>
						</#list>
					</tbody>
				</table>
			</div>
			<div class="col">
				<#list page.content as author>
				<div class="d-none detail" id="${author.id?c}">
					<table id="detailTable" class="table caption-top w-autox">
						<thead>
							<tr><th colspan="3" class="text-center">Authors Books</th></tr>
						</thead>
						<tbody>
							<#list author.books as book>
							<tr>
								<td scope="row">${book.id!'-'}</td>
								<td scope="row">
									<#assign link = "/fm/books?page=0&search=${book.title}">
									<a href="${link}">${book.title!'-'}</a>
								</td>
								<td scope="row">
									<#if book.serie?has_content>
										<#assign link = "/fm/books?page=0&sort=seriesIndex&search=serie:${book.serie.name!''}">
										<a href="${link}">${book.serie.name!'-'}</a> ${book.seriesIndex}
									</#if>
								</td>
							</tr>
							</#list>
						</tbody>
					</table>
				</div>	
				</#list>
			</div>
		</div>
	</div>
*/