import React from "react";
import { Link } from "react-router-dom";

const client = require('./client');

import { Helmet } from 'react-helmet';
import { Paginator, Navbar } from "./utils";

export class MasterDetailPage extends React.Component {

	constructor(props) {
		super(props);
		this.relName = props.relName;
		this.pathName = props.pathName;
		this.state = {
				records: [], detail: {}, links: {}, page: {size: 15, number: 0, totalPages: 0, totalElements: 0}
		};
		/**
		 * relName and pathName see: spring boot @RepositoryRestResource
		 */
		console.log(this.pathName + " constructor", props, this.pathName, this.relName, this.state);
	}

	dataName() {
		return this.pathName ? this.pathName.charAt(0).toUpperCase() + this.pathName.slice(1) : '';
	}
	
	getData(path, params) {
		client({method: 'GET', path: path, params: params}).done(response => {
			console.log(response.entity);
			if (response.entity._embedded.hasOwnProperty(this.relName)) {
				var state = {
					records: response.entity._embedded[this.relName],
					detail: (response.entity._embedded[this.relName].length > 0) ? response.entity._embedded[this.relName][0] : {},
					page: response.entity.page, 
					links: response.entity._links
				};
				this.setState(state);
				console.log('State: ', this.state)
				this.dataLoaded();
			} else {
				console.log('Keine ' + this.relName + ' Daten', response.entity)
			}
				
		});
	}

	markRecord() {
		$('#mainTable > tbody > tr').removeClass('table-success')
		$('#mainTable > tbody > tr[data-id=' + this.state.detail.id + ']').addClass('table-success');
	}
	
	dataLoaded() {
		console.log(this.dataName() + " dataLoaded");
		
		$('.navbar-nav > li > a').removeClass('active')
		var selector = '.navbar-nav > li > a[id=' + this.pathName + ']'
		$(selector).addClass('active')
		
		if(this.state.detail) {
			this.markRecord();
		}
	}

	showdetail(e, id) {
		console.log(this.dataName() + " showdetail", id, e);
		this.markRecord();
		this.setState({
			detail: this.state.records.find(data => data.id == id)
		});
	}
	
	componentDidMount() { 
		console.log(this.dataName() + " componentDidMount");
		this.getData('/api/' + this.pathName, {size: this.state.page.size, page: this.state.page.number, sort: this.state.page.sort});
	}

	gotopage(pagenumber) {
		console.log(this.dataName() + " gotopage", this.state);
		this.getData('/api/' + this.pathName, {size: this.state.page.size, page: pagenumber, sort: this.state.page.sort});
	}

	search(search, field) {
		console.log(this.dataName() + " search", field, search, this.state.page);

		var sort = this.state.page.sort
		if (! field) {
			var columnIndex = search.indexOf(':');
			if (columnIndex > 0) {
				field = search.substring(0,columnIndex);
				search = search.substring(columnIndex + 1);
			}
		}	

//		var baseUrl = '/api/' + this.relName + 's/search/'	
		var baseUrl = '/api/books/search/' // Egal von welcher Seite, es wird immer nach Büchern gesucht.	
		var booksearchUrl = baseUrl + 'findByTitleLike'
		if (field) {
			if (field == 'serie') {
				booksearchUrl = baseUrl + 'findBySeriesNameLike'
				sort = 'seriesIndex'
			}
			if (field == 'author') {
				booksearchUrl = baseUrl + 'findByAuthorsSortLike'
				sort = 'title'
			}
			if (field == 'tag') {
				booksearchUrl = baseUrl + 'findByTagsNameLike'
			}
		}
		
		this.getData(booksearchUrl, {size: this.state.page.size, page: 0, sort: sort, name: '%' + search + '%'});
	}

	handleClick(event, value) {
		console.log(this.dataName() + ' handleClick', event, value);
		this.search(value, this.relName);
	    event.preventDefault();
	}
	
	render() {
		console.log(this.dataName()  + "Page render Props", this.props, 'State', this.state);
		if (this.props.fields && this.state.records) {
			return (
				<div>
					<Helmet><title>{this.dataName()} (react)</title></Helmet>
					<Navbar/>
					<Paginator page = {this.state.page} gotopage = {this.gotopage.bind(this)} />	
					<div className="row align-items-start">
						<div className="col">
							<table id="mainTable" className="table table-striped table-hover caption-top">
							<thead className="bg-gradient-info">
								<tr>{this.props.fields.map((field, index) => { return (<td key={index} scope="col">{field.l}</td>); })}</tr>
							</thead>
							<tbody>
								{this.state.records.map(record => {
									return (
										<tr key={record.id} data-id={record.id} onClick={(e) => this.showdetail(e, record.id)} >
											{this.props.fields.map(field => <td scope="row">{record[field.f]}</td>)}
										</tr>
									);
								})}
							</tbody>
						</table>
						</div>
						<div className="col">
							<div  id={this.state.detail.id} className="detail" >
								<table id="detailTable" className="table caption-top w-autox bookdetail" >
									<thead>
										<tr><th colSpan="3" className="text-center">{this.relName} Detail</th></tr>
									</thead>
									<tbody>
										{this.props.fields.map(field => {
											if (field.f == 'name') {
												return (<tr key={field.f}><td>{field.l}</td><td>
														<a onClick={(e) => this.handleClick(e,this.state.detail[field.f])} href="">{this.state.detail[field.f]}</a>
													</td></tr>);
											}
											return (<tr key={field.f}><td>{field.l}</td><td>{this.state.detail[field.f]}</td></tr>);
										})}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>	
			)
		}
		console.log(this.relName + "Page render WRONG Props", this.props.fields, 'State', this.state[this.pathName]);
		return (<div/>)
	}

}
