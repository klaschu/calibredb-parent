import React from "react";
import { Link } from "react-router-dom";

const client = require('./client');

import { Helmet } from 'react-helmet';

export class Navbar extends React.Component {
	
	constructor(props) {
		super(props);
	    this.state = {value: ''};
	    this.handleChange = this.handleChange.bind(this);
	    this.handleSubmit = this.handleSubmit.bind(this);
		console.log('Navbar constructor', props, this.state);
	}

	componentDidMount() {
		console.log('Navbar componentDidMount')
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('Navbar componentDidUpdate', prevProps, prevState, snapshot)
	}
	
	componentWillUnmount() {
		console.log('Navbar componentWillUnmount')
	}
	
	handleChange(event) {    
		console.log('Navbar handleChange', event)
		this.setState({value: event.target.value});  
	}

	handleSubmit(event) {
		console.log('Navbar handleSubmit', event)
		if (this.props.search) {
			this.props.search(this.state.value);
			event.preventDefault();
	    }
	}
	
	render() {
		console.log('Navbar render')
		return (
			<nav className="navbar navbar-expand-lg navbar-light">
			  <div className="container-fluid">
			    <button id="nav-btn" className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDiv" aria-expanded="false" aria-label="Toggle navigation">
		        	<span className="navbar-toggler-icon"></span>
		        </button>
			    <div className="collapse navbar-collapse" id="navbarDiv">
			      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
			        <li className="nav-item"><Link id="books" 	className="nav-link" aria-current="page" to="/books">Books</Link></li>
			        <li className="nav-item"><Link id="authors" className="nav-link" to="/authors">Authors</Link></li>
			        <li className="nav-item"><Link id="series"  className="nav-link" to="/series">Series</Link></li>
			        <li className="nav-item"><Link id="tags"    className="nav-link" to="/tags">Tags</Link></li>
			      </ul>
			      <form className="d-flex" onSubmit={this.handleSubmit} method="get">
			        <input className="form-control me-2" type="search" name="search" placeholder="Search" aria-label="Search" value={this.state.value} onChange={this.handleChange} ></input>
			        <button className="btn btn-outline-success" type="submit" value="Submit">Search</button>
			      </form>
			    </div>
			  </div>
			</nav>
		)
	}
}

export class Paginator extends React.Component {
	
	constructor(props) {
		  super(props);
		  this.state = {
				  data: {items:[]}
		  };
		  this.handleClick = this.handleClick.bind(this);
		  console.log('Paginator constructor', props, this.state)
	}

	calcPaginator() {
		console.log("Paginator calcPaginator", this.state);
		client({method: 'GET', path: '/api/util/paginator', params: {currentPage: this.props.page.number, totalPages: this.props.page.totalPages, totalElements: this.props.page.totalElements} }).done(response => {
			this.setState({
				data: response.entity
			});
			console.log('Paginator GET Result', this.state.data)
		});
	}
	
	componentDidMount() {
		console.log('Paginator componentDidMount', this.state, this.props)
		this.calcPaginator();
	}
	
	pageObjectIsChanged(prevPage, newPage) {
		if (prevPage.number != newPage.number) {
			return true;
		}
		if (prevPage.totalPages != newPage.totalPages) {
			return true;
		}
		if (prevPage.totalElements != newPage.totalElements) {
			return true;
		}
		return false;
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		console.log('Paginator componentDidUpdate Props', prevProps, this.props, 'State', prevState, this.state)
		if (this.pageObjectIsChanged(prevProps.page, this.props.page)) {
			this.calcPaginator();
		}
	}
	
	componentWillUnmount() {
		console.log('Paginator componentWillUnmount')
	}

	handleClick(event, page) {
		console.log('Paginator handleClick', page);
		this.props.gotopage(page);
//	    event.preventDefault();
	}
	
	render() {
		console.log('Paginator render')
//		console.log('Paginator  render', this.props.paginator.items)
		const listElements = this.state.data.items.map(item => <li key={item.id} className={item.cssClass}><a className="page-link" onClick={(e) => this.handleClick(e,item.page)} >{item.title}</a></li>);
		return (
			<div className="pagination">
				<ul className="pagination mt-2">	
					{listElements}
				</ul>
				<div className="d-flex align-items-center">&nbsp; {this.props.page.totalElements} Records</div>
			</div>
		)
	}
}
