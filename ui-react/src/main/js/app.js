'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { BooksPage } from "./books";
import { AuthorsPage } from "./authors";
import { SeriesPage } from "./series";
import { TagsPage } from "./tags";

const bookFields = [{l:'Id',f:'id'}, {l:'Title',f:'title'}, {l:'Author',f:'authorSort'}];
const authorFields = [{l:'Id',f:'id'}, {l:'Name',f:'name'}, {l:'# Books',f:'bookCount'}];
const serieFields = [{l:'Id',f:'id'}, {l:'Name',f:'name'}, {l:'# Books',f:'bookCount'}];
const tagFields = [{l:'Id',f:'id'}, {l:'Name',f:'name'}, {l:'# Books',f:'bookCount'}];
const bookProps = {relName: 'book', pathName: 'books', fields: [{l:'Id',f:'id'}, {l:'Title',f:'title'}, {l:'Author',f:'authorSort'}]}

const routing = (
  <BrowserRouter basename={'/ui/react'}>
    <Switch>
      <Route exact path="/" render={() => <BooksPage props = {bookProps}/>} />
      <Route path="/books" render={() => <BooksPage relName = 'book' pathName = 'books' fields = {bookFields} />} />
      <Route path="/authors" render={() => <AuthorsPage relName = 'author' pathName = 'authors' fields = {authorFields} />} />
      <Route path="/series" render={() => <SeriesPage relName = 'serie' pathName = 'series' fields = {serieFields} />} />
      <Route path="/tags" render={() => <TagsPage relName = 'tag' pathName = 'tags' fields = {tagFields} />} />
    </Switch>
  </BrowserRouter>
)

ReactDOM.render(routing, document.getElementById('react'))

