'use strict';
const React= require('react');
const client = require('./client');

import { Helmet } from 'react-helmet';
import { Paginator, Navbar } from "./utils";
import { MasterDetailPage } from "./masterdetail";
import { BookDetail } from "./bookdetail";

export class BooksPage extends MasterDetailPage {

	constructor(props) {
		super(props)
		console.log("BooksPage constructor", props, this.state);
	}
	
	render() {
		console.log("BooksPage render Props", this.props, 'State', this.state);
		return (
			<div>
				<Helmet><title>Books (react)</title></Helmet>
				<Navbar/>
				<Paginator page = {this.state.page} gotopage = {this.gotopage.bind(this)} />	
				<div className="row align-items-start">
					<div className="col">
						<BookList books = {this.state.records} page = {this.state.page} showdetail = {this.showdetail.bind(this)}/>
					</div>
					<div className="col"> 
						<BookDetail book = {this.state.detail} search = {this.search.bind(this)} />
					</div>
				</div>
			</div>	
		)
	}
}

export class BookList extends React.Component {
	render() {
		console.log("BookList render");
//		console.log("BookList render", this.props);
		const books = this.props.books.map(book => <Book key={book.id} book={book} showdetail={this.props.showdetail}/>); 
		return (
			<table id="mainTable" className="table table-striped table-hover caption-top">
				<thead className="bg-gradient-info">
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Title</th>
						<th scope="col">Author</th>
					</tr>
				</thead>
				<tbody>
					{books}
				</tbody>
			</table>
		)
	}
}

class Book extends React.Component{
	render() {
		console.log("Book render");
//		console.log("Book render");
		return (
			<tr data-id={this.props.book.id} onClick={(e)  => this.props.showdetail(e, this.props.book.id)} >
				<td scope="row">{this.props.book.id}</td>
				<td scope="row">{this.props.book.title}</td>
				<td scope="row">{this.props.book.authorSort}</td>
			</tr>
		)
	}
}
