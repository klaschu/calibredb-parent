package de.klaschu.epub.db.calibre.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
    @Value("${ui.react.base-path}") 
	private String basePath;
	
	/**
	 * Siehe: <a href="https://www.baeldung.com/spring-redirect-and-forward#forward-with-the-prefix-forward">forward-with-the-prefix-forward</a>
	 */
	@Override
	  public void addViewControllers(ViewControllerRegistry registry) {
	    registry.addViewController(basePath + "/").setViewName(String.format("forward:%s/books/index.html", basePath));
	    registry.addViewController(basePath + "/books").setViewName(String.format("forward:%s/books/index.html", basePath));
	    registry.addViewController(basePath + "/auhtors").setViewName(String.format("forward:%s/books/index.html", basePath));
	    registry.addViewController(basePath + "/series").setViewName(String.format("forward:%s/books/index.html", basePath));
	    registry.addViewController(basePath + "/tags").setViewName(String.format("forward:%s/books/index.html", basePath));
	  }
}