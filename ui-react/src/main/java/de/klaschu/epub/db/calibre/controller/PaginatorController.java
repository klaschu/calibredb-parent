package de.klaschu.epub.db.calibre.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaginatorController {
	private static final Logger log = LoggerFactory.getLogger(PaginatorController.class);

	@Value("${paginator.segment.size}")
	private Integer segmentSize;

	@GetMapping("/api/util/paginator")
	public Paginator getPaginator(@RequestParam Integer currentPage, @RequestParam Integer totalPages, @RequestParam(required = false) Long totalElements) {
		log.debug("getPaginator currentPage {}, totalPages {}, totalElements {}", currentPage, totalPages, totalElements);
		
		return Paginator.create(currentPage, totalPages, totalElements, segmentSize, "page=%s");
	}
}
