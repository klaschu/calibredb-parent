String Boot JPA Beispiel mit Calibre SQLite DB und div. UIs 
=========

Das Projekt versucht nicht Calibre Konkurrenz zu machen sondern ist eher als eine Studie zu Spring Boot zu sehen.

Folgende Ideen dazu: 

- Anbindung der SQLite Datenbank von Calibre mit JPA. **Done.**
- Bereitstellen der Daten als REST Api. **Done.**
- API Dokumentation mit [Swagger](https://swagger.io) erstellen. **Done.**
- Web Frontend mit [ReactJs](https://reactjs.org) **Done.**
- Web Frontend mit [Freemarker](https://freemarker.apache.org) Template **Done.**
- Schön machen der Febfrontend mit [Bootstrap](https://getbootstrap.com/).
- Web Frontend mit [EmberJs](https://emberjs.com/), [AngularJs](https://angularjs.org/), [VueJs](https://vuejs.org), [BackboneJs](https://backbonejs.org), 
- App Frontend mit [JavaFX](https://openjfx.io/).

## History
- 07.01.2021 Initiales Projekt erstellt. Zugriff auf DB, REST Api. 
- 13.01.2021 Swagger, React. 
- 14.01.2021 Pagination zugefügt.
- 15.01.2021 Web Frontend mit Freemarker Template erstellt. 
- 23.01.2021 Projekt neu organisiert. Aufteilung auf Teilprojekte. 

*Weitere Historie: Siehe [Git Repository](https://gitlab.com/klaschu/calibredb-parent)*
